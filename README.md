PallMall
========

Links
-----

Homepage: https://tranquil-sea-85374.heroku.com/

Source: https://gitlab.com/adamelso/pallmall


Quickstart
----------

    $ git clone git@gitlab.com:adamelso/pallmall.git
    $ cd pallmall
    ### Edit database credentials in the .env file.
    $ vim .env
    $ make install
    $ make test
    $ make serve
    
    [OK] Server listening on http://127.0.0.1:8000                                                                         

    $ bin/console server:stop


Features
-------

 * Add co-authors.
 * Add multiple affiliations while creating a co-author.
 * Edit affiliation.


Possible Todos
--------------

 * Add affiliations to existing authors.
 * Delete an affililiation.


Tools
-----

 * Symfony Flex - As the framework.
 * Doctrine ORM - For database management.
 * Doctrine Fixtures - For resetting the database for the demo.
 * Symfony Console - For resetting the database via command line.
 * Monolog - For logging on Heroku.
 * Symfony PSR-7 Bridge + Zend Diactoros
 * PhpSpec - for unit testing.
 * Bootstrap - for the frontend.
 
Design Choices
-----

### PSR-7 ###
 
Allows some controllers to be used outside of the Symfony framework.
There's unfortunately no PSR-7 FormHandler for Symfony Forms yet,
so not all controllers are using it.


### Explicit Dependencies for Controllers ###

The controllers do not extend the FrameworkBundle base Controller, allowing
for portability, and usage outside of the
Symfony Framework.


### Invokable Controllers ###

Rather than a controller having multiple responsibilities, these are split
one per controller. This allows for dependencies to be more explicitly
defined, and conforms to HTTP Middleware signatures (pending PSR-15).


### Partial Template Loading ###
 
In order to refresh the content asynchronously, some AJAX requests will
also return a partial HTMl response to be embedded on the page. These partials
are shared between being rendered for these responses and by Twig when including
them on the initial page load.


### RESTful, but not JSON ###

REST is implemented in the sense that URLs are represented by the HTTP Verbs and
status code responses. However, a full JSON REST API is not implemented for any
resource type.
