// @todo Use Backbone ?

var Flash = function (type, message) {
    this.type = type;
    this.message = message;
};

Flash.prototype.render = function () {
    var self = this;

    $close = $('<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>');
    $flash = $('<li class="alert alert-dismissible alert-'+self.type+'" role="alert"></li>');

    $flash.prepend($close);
    $flash.append(self.message);
    $flash.alert('close');

    return $flash;
};

var getResetDemoForm = function () {
    return $('#pallmall').attr('data-reset-demo-form');
};

var createAuthorEndpoint = function () {
    return $('#pallmall').attr('data-author-endpoint');
};

var displaySuccessFlash = function (message) {
    var flash = new Flash('success', message);

    $('#flashes').append(flash.render());
};

var createCancelAffiliationButton = function () {
    $button = $('<button data-cancel-affiliation class="btn btn-default"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> Cancel</button>');

    $button.on('click', function (e) {
        e.preventDefault();
        $li = $(this).parent('li');
        $li.remove();
    });

    return $button;
};

var createCancelEditAffiliationButton = function ($element, previousHtml) {
    $button = $('<button data-cancel-affiliation class="btn btn-default"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> Cancel</button>');

    $button.on('click', function (e) {
        e.preventDefault();
        $element.html(previousHtml);

        $('[data-edit-affiliation-form]', $element).on('click', editAffiliationButtonClicked);
    });

    return $button;
};

var renderEditAffiliationForm = function ($element, getFormUrl, formActionUrl) {
    var refresh = function (data) {
        $element.closest('.panel').replaceWith(data);
    };

    var appendFormAndBindEvents = function (data) {
        previousHtml = $element.html();

        $element.html(data)
            .append(createCancelEditAffiliationButton($element, previousHtml));

        $('form.edit-affiliation-form', $element).on('submit', function (e) {
            e.preventDefault();

            $.post(formActionUrl, $(this).serialize()).done(refresh);
        });
    };

    $.get(getFormUrl).done(appendFormAndBindEvents);
};

var addAffiliationForm = function ($collectionHolder, $buttonContainer) {
    var prototype = $collectionHolder.attr('data-affiliations');
    var index = $collectionHolder.attr('data-index');
    var newForm = prototype.replace(/__name__/g, index);

    $collectionHolder.data('index', index + 1);

    var $addAffiliationFormContainer = $('<li class="panel panel-default"></li>')
        .append(newForm)
        .append(createCancelAffiliationButton());

    $buttonContainer.before($addAffiliationFormContainer);
};

var createAddAffiliationButton = function ($collectionHolder, $buttonContainer) {
    $button = $('<button id="addAffiliation" class="btn btn-default"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Add Affiliation</button>');

    $button.on('click', function (e) {
        e.preventDefault();
        addAffiliationForm($collectionHolder, $buttonContainer);
    });

    return $button;
};

var editAffiliationButtonClicked = function (e) {
    var $elem = $(this);

    var $context = $elem.closest('.affiliation-body');

    renderEditAffiliationForm(
        $context,
        $elem.attr('data-edit-affiliation-form'),
        $elem.attr('data-update-affiliation')
    );
};

var bindListenersToEditAffiliationButtons = function () {
    $('[data-edit-affiliation-form]').on('click', editAffiliationButtonClicked);
};

var renderFormCollection = function ($collectionHolder) {
    var $buttonContainer = $('<li></li>');
    $buttonContainer.append(createAddAffiliationButton($collectionHolder, $buttonContainer));

    $collectionHolder.append($buttonContainer);
    $collectionHolder.attr('data-index', $collectionHolder.find(':input').length);

    var onSuccessSubmittingNewCoAuthor = function (data) {
        $('#addCoAuthorPanel').modal('hide');

        displaySuccessFlash('New author was successfully created.');

        document.getElementById('authorsPanel').innerHTML = data;

        bindListenersToEditAffiliationButtons();
    };

    /**
     * The entire form HTML is replaced with a new one on error.
     *
     * This has the added benefit of both rendering form errors in their correct place and renewing the CSRF token.
     *
     * @param {jQuery.Deferred|jqXHR} jqXHR
     * @param {string} textStatus
     * @param {string} errorThrown
     */
    var onFailureSubmittingNewCoAuthor = function (jqXHR, textStatus, errorThrown) {
        var flash = new Flash('danger', 'The co-author could not be created. Please check the form.');

        $flashes = $('<ul />');
        $flashes.append(flash.render());

        $('#addCoAuthorForm').html($flashes.prop('outerHTML') + jqXHR.responseText);
    };

    var submitCoAuthor = function (e) {
        e.preventDefault();

        $.post(createAuthorEndpoint(), $(this).serialize())
            .done(onSuccessSubmittingNewCoAuthor)
            .fail(onFailureSubmittingNewCoAuthor);
    };

    // @todo author_submit ID is named by Symfony, hence not adhering to other naming conventions.
    document.getElementById('addCoAuthorForm').onsubmit = submitCoAuthor;
};

var getResetDemoConfirmation = function () {
    var chrisTarrant = '<p>Are you sure you want to reset the demo and delete everything ?</p>';

    return chrisTarrant + getResetDemoForm();
};

var main = function () {
    $('#resetDemoButton').popover({
        'content': getResetDemoConfirmation,
        'html': true,
        'placement': 'bottom'
    });

    renderFormCollection($('[data-affiliations]'));

    bindListenersToEditAffiliationButtons();
};

$(main);
