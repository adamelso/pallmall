<?php

namespace spec\PallMall\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use PallMall\Controller\CreateAuthorController;
use PallMall\Form\Type\AuthorType;
use PallMall\Model\Author;
use PallMall\Repository\AuthorRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CreateAuthorControllerSpec extends ObjectBehavior
{
    function let(\Twig_Environment $twig, FormFactoryInterface $formFactory, ObjectManager $objectManager, AuthorRepository $repository)
    {
        $this->beConstructedWith($twig, $formFactory, $objectManager, $repository);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(CreateAuthorController::class);
    }

    function it_creates_a_new_author_from_the_submitted_form_and_returns_an_updated_list_of_authors(Request $request, \Twig_Environment $twig, FormFactoryInterface $formFactory, ObjectManager $objectManager, AuthorRepository $repository, FormInterface $form, Author $author, Author $anotherAuthor)
    {
        $formFactory->create(AuthorType::class)->willReturn($form);
        $form->handleRequest($request)->shouldBeCalled();
        $form->isSubmitted()->willReturn(true);
        $form->isValid()->willReturn(true);

        $form->getData()->willReturn($author);

        $objectManager->persist($author)->shouldBeCalled();
        $objectManager->flush()->shouldBeCalled();
        $repository->findAll()->willReturn([$author, $anotherAuthor]);

        $twig->render('authors.html.twig', ['authors' => [$author, $anotherAuthor]])->willReturn('<ul id="authors">...</ul>');

        $this($request)->shouldBeLike(new Response('<ul id="authors">...</ul>'));
    }

    function it_renders_a_new_form_and_feedbacks_errors_if_the_submitted_form_cannot_be_processed(Request $request, \Twig_Environment $twig, FormFactoryInterface $formFactory, ObjectManager $objectManager, AuthorRepository $repository, FormInterface $form, FormView $formView)
    {
        $formFactory->create(AuthorType::class)->willReturn($form);
        $form->handleRequest($request)->shouldBeCalled();
        $form->isSubmitted()->willReturn(true);
        $form->isValid()->willReturn(false);

        $form->createView()->willReturn($formView);

        $objectManager->persist(Argument::any())->shouldNotBeCalled();
        $objectManager->flush()->shouldNotBeCalled();
        $repository->findAll()->shouldNotBeCalled();

        $twig->render('create_author_form.html.twig', ['coAuthorForm' => $formView])->willReturn('<form id="createAuthor">...</form>');

        $this($request)->shouldBeLike(new Response('<form id="createAuthor">...</form>', 422));
    }

    function it_returns_an_error_response_if_the_form_could_not_be_submitted(Request $request, \Twig_Environment $twig, FormFactoryInterface $formFactory, ObjectManager $objectManager, AuthorRepository $repository, FormInterface $form)
    {
        $formFactory->create(AuthorType::class)->willReturn($form);
        $form->handleRequest($request)->shouldBeCalled();
        $form->isSubmitted()->willReturn(false);

        $objectManager->persist(Argument::any())->shouldNotBeCalled();
        $objectManager->flush()->shouldNotBeCalled();
        $repository->findAll()->shouldNotBeCalled();
        $twig->render(Argument::any())->shouldNotBeCalled();

        $this($request)->shouldBeLike(new Response('Form could not be submitted.', 500));
    }
}
