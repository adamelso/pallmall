<?php

namespace spec\PallMall\Controller;

use PallMall\Controller\IndexController;
use PallMall\Form\Type\AuthorType;
use PallMall\HttpMessage\ResponseFactory;
use PallMall\Model\Author;
use PallMall\Repository\AuthorRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class IndexControllerSpec extends ObjectBehavior
{
    function let(\Twig_Environment $twig, FormFactoryInterface $formFactory, AuthorRepository $authorRepository, ResponseFactory $responseFactory)
    {
        $this->beConstructedWith($twig, $formFactory, $authorRepository, $responseFactory);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(IndexController::class);
    }

    function it_renders_the_index_of_authors_and_their_affiliations(ServerRequestInterface $request, ResponseInterface $response, \Twig_Environment $twig, FormFactoryInterface $formFactory, AuthorRepository $authorRepository, ResponseFactory $responseFactory, Author $alice, Author $bob, Author $chris, FormInterface $createAuthorForm, FormView $createAuthorFormView, FormInterface $resetButton, FormView $resetButtonView)
    {
        $authorRepository->findBy([], ['lastName' => 'asc'])->willReturn([$alice, $bob, $chris]);

        $formFactory->create(AuthorType::class, new Author())->willReturn($createAuthorForm);
        $createAuthorForm->createView()->willReturn($createAuthorFormView);

        $formFactory->createNamed('reset_demo', FormType::class)->willReturn($resetButton);
        $resetButton->add('reset', SubmitType::class, ['attr' => ['class' => 'btn-danger']])->shouldBeCalled();
        $resetButton->createView()->willReturn($resetButtonView);

        $twig->render('index.html.twig', [
            'coAuthorForm' => $createAuthorFormView,
            'authors' => [$alice, $bob, $chris],
            'resetDemoForm' => $resetButtonView,
        ])->willReturn('<!doctype html><html>...</html>');

        $responseFactory->createHtmlResponse('<!doctype html><html>...</html>')->willReturn($response);

        $this($request)->shouldReturn($response);
    }
}
