<?php

namespace spec\PallMall\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use PallMall\Controller\UpdateAffiliationController;
use PallMall\Form\Type\AffiliationType;
use PallMall\HttpMessage\ResponseFactory;
use PallMall\Model\Affiliation;
use PallMall\Repository\AffiliationRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UpdateAffiliationControllerSpec extends ObjectBehavior
{
    function let(\Twig_Environment $twig, FormFactoryInterface $formFactory, ObjectManager $objectManager, AffiliationRepository $repository, ResponseFactory $responseFactory)
    {
        $this->beConstructedWith($twig, $formFactory, $objectManager, $repository, $responseFactory);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(UpdateAffiliationController::class);
    }

    function it_updates_an_affiliation_and_returns_the_rendered_view(Request $request, \Twig_Environment $twig, FormFactoryInterface $formFactory, ObjectManager $objectManager, AffiliationRepository $repository, ResponseFactory $responseFactory, ParameterBag $attributes, ParameterBag $body, Affiliation $affiliation, FormInterface $form)
    {
        $request->attributes = $attributes;
        $request->request = $body;
        $attributes->get('id')->willReturn(42);
        $body->get('edit_affiliation')->willReturn(['institution_name' => 'Trump University']);
        $form->getName()->willReturn('edit_affiliation');

        $repository->find(42)->willReturn($affiliation);
        $formFactory->create(AffiliationType::class, $affiliation)->willReturn($form);
        $form->submit(['institution_name' => 'Trump University'])->shouldBeCalled();

        $form->isSubmitted()->willReturn(true);
        $form->isValid()->willReturn(true);

        $objectManager->flush()->shouldBeCalled();
        $twig->render('affiliation.html.twig', ['affiliation' => $affiliation])->willReturn('<div>...</div>');

        $this($request)->shouldBeLike(new Response('<div>...</div>'));
    }

    function it_returns_form_errors_if_the_submitted_form_is_not_valid(Request $request, FormFactoryInterface $formFactory, ObjectManager $objectManager, AffiliationRepository $repository, ResponseFactory $responseFactory, ParameterBag $attributes, ParameterBag $body, Affiliation $affiliation, FormInterface $form)
    {
        $request->attributes = $attributes;
        $request->request = $body;
        $attributes->get('id')->willReturn(42);
        $body->get('edit_affiliation')->willReturn(['institution_name' => 'Trump University']);
        $form->getName()->willReturn('edit_affiliation');

        $repository->find(42)->willReturn($affiliation);
        $formFactory->create(AffiliationType::class, $affiliation)->willReturn($form);
        $form->submit(['institution_name' => 'Trump University'])->shouldBeCalled();

        $form->isSubmitted()->willReturn(true);
        $form->isValid()->willReturn(false);

        $form->getErrors(true)->willReturn('Form errors are...');

        $objectManager->flush()->shouldNotBeCalled();

        $this($request)->shouldBeLike(new Response('Form was not valid. Form errors are...', 422));
    }
}
