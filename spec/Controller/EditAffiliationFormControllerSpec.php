<?php

namespace spec\PallMall\Controller;

use PallMall\Controller\EditAffiliationFormController;
use PallMall\Form\Type\AffiliationType;
use PallMall\HttpMessage\ResponseFactory;
use PallMall\Model\Affiliation;
use PallMall\Repository\AffiliationRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Zend\Diactoros\Response;
use Zend\Diactoros\Stream;

class EditAffiliationFormControllerSpec extends ObjectBehavior
{
    function let(\Twig_Environment $twig, FormFactoryInterface $formFactory, AffiliationRepository $repository, ResponseFactory $responseFactory)
    {
        $this->beConstructedWith($twig, $formFactory, $repository, $responseFactory);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(EditAffiliationFormController::class);
    }

    function it_returns_an_edit_form_for_a_given_affiliation(ServerRequestInterface $request, ResponseInterface $response, \Twig_Environment $twig, FormFactoryInterface $formFactory, AffiliationRepository $repository, ResponseFactory $responseFactory, Affiliation $affiliation, FormInterface $form, FormView $formView)
    {
        $request->getAttribute('id')->willReturn(42);
        $repository->find(42)->willReturn($affiliation);
        $formFactory->create(AffiliationType::class, $affiliation)->willReturn($form);

        $form->createView()->willReturn($formView);
        $twig->render('edit_affiliation_form.html.twig', ['affiliationForm' => $formView])->willReturn('<form id="editAffiliation">...</form>');

        $responseFactory->createHtmlResponse('<form id="editAffiliation">...</form>')->willReturn($response);

        $this($request)->shouldReturn($response);
    }

    function it_does_not_return_a_form_for_a_non_existent_affiliation(ServerRequestInterface $request, \Twig_Environment $twig, FormFactoryInterface $formFactory, AffiliationRepository $repository)
    {
        $request->getAttribute('id')->willReturn(9001);
        $repository->find(9001)->willReturn(null);

        $formFactory->create(Argument::any())->shouldNotBeCalled();
        $twig->render(Argument::any())->shouldNotBeCalled();

        $this->shouldThrow(NotFoundHttpException::class)->during('__invoke', [$request]);
    }
}
