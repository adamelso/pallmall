<?php

return [
    'Symfony\Bundle\FrameworkBundle\FrameworkBundle' => ['all' => true],
    'Symfony\Bundle\WebServerBundle\WebServerBundle' => ['dev' => true],
    'Doctrine\Bundle\DoctrineCacheBundle\DoctrineCacheBundle' => ['all' => true],
    'Doctrine\Bundle\DoctrineBundle\DoctrineBundle' => ['all' => true],
    'Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle' => ['all' => true],
    'Symfony\Bundle\TwigBundle\TwigBundle' => ['all' => true],
    'Symfony\Bundle\WebProfilerBundle\WebProfilerBundle' => ['dev' => true, 'test' => true],
    'Symfony\Bundle\MonologBundle\MonologBundle' => ['all' => true],
];
