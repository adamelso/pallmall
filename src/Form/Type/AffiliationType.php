<?php

namespace PallMall\Form\Type;

use PallMall\Model\Affiliation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AffiliationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('institutionName');
        $builder->add('address');
        $builder->add('city');
        $builder->add('country', CountryType::class, [
            'preferred_choices' => ['GB'],
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Affiliation::class,
            'attr' => [
                'class' => 'panel-body',
            ]
        ]);
    }
}
