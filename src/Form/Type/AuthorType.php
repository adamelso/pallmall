<?php

namespace PallMall\Form\Type;

use PallMall\Model\Author;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AuthorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, ['label' => 'First name']);
        $builder->add('lastName');
        $builder->add('email', EmailType::class);
        $builder->add('affiliations', CollectionType::class, [
            'entry_type' => AffiliationType::class,
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,
            'label' => false,
            'by_reference' => false,
        ]);
        $builder->add('submit', SubmitType::class, [
            'attr' => [
                'class' => 'btn-primary',
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Author::class,
        ]);
    }
}
