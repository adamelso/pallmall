<?php

namespace PallMall\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Adam Elsodaney <adam.elso@gmail.com>
 *
 * @ORM\Entity(repositoryClass="PallMall\Repository\AuthorRepository")
 */
class Author
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * First Name.
     *
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Email()
     */
    private $email;

    /**
     * @var Affiliation[]|Collection
     *
     * @ORM\OneToMany(targetEntity="PallMall\Model\Affiliation", mappedBy="author", cascade={"persist"})
     * @Assert\Count(min=1, minMessage = "You must specify at least one affiliation")
     */
    private $affiliations;

    public function __construct()
    {
        $this->affiliations = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return "{$this->name} {$this->lastName}";
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return Collection|Affiliation[]
     */
    public function getAffiliations()
    {
        return $this->affiliations;
    }

    /**
     * @param Affiliation $affiliation
     */
    public function addAffiliation(Affiliation $affiliation)
    {
        $affiliation->setAuthor($this);
        $this->affiliations[] = $affiliation;
    }

    /**
     * @param Affiliation $affiliation
     */
    public function removeAffiliation(Affiliation $affiliation)
    {
        $affiliation->setAuthor(null);
        $this->affiliations->removeElement($affiliation);
    }
}
