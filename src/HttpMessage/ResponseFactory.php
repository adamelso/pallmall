<?php

namespace PallMall\HttpMessage;

use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response;
use Zend\Diactoros\Stream;

class ResponseFactory
{
    /**
     * @param string $data
     *
     * @return ResponseInterface
     */
    public function createHtmlResponse(string $data): ResponseInterface
    {
        $body = new Stream('php://memory', 'wb');
        $body->write($data);

        return new Response($body);
    }
}
