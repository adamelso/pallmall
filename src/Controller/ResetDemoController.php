<?php

namespace PallMall\Controller;

use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Zend\Diactoros\Response\RedirectResponse;

class ResetDemoController
{
    /**
     * @var ORMPurger
     */
    private $purger;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    public function __construct(ORMPurger $purger, UrlGeneratorInterface $urlGenerator)
    {
        $this->purger = $purger;
        $this->urlGenerator = $urlGenerator;
    }

    public function __invoke(ServerRequestInterface $request)
    {
        $this->purger->purge();

        return new RedirectResponse($this->urlGenerator->generate('index'));
    }
}
