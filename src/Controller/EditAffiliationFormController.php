<?php

namespace PallMall\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use PallMall\Form\Type\AffiliationType;
use PallMall\HttpMessage\ResponseFactory;
use PallMall\Model\Affiliation;
use PallMall\Repository\AffiliationRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Zend\Diactoros\Response;
use Zend\Diactoros\Stream;

class EditAffiliationFormController
{
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var AffiliationRepository
     */
    private $repository;
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * @param \Twig_Environment $twig
     * @param FormFactoryInterface $formFactory
     * @param AffiliationRepository $repository
     * @param ResponseFactory $responseFactory
     */
    public function __construct(\Twig_Environment $twig, FormFactoryInterface $formFactory, AffiliationRepository $repository, ResponseFactory $responseFactory)
    {
        $this->twig = $twig;
        $this->formFactory = $formFactory;
        $this->repository = $repository;
        $this->responseFactory = $responseFactory;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request)
    {
        $id = $request->getAttribute('id');

        $affiliation = $this->repository->find($id);

        if (! $affiliation) {
            throw new NotFoundHttpException('Affiliation not found.');
        }

        $form = $this->formFactory->create(AffiliationType::class, $affiliation);

        return $this->responseFactory->createHtmlResponse($this->twig->render('edit_affiliation_form.html.twig', [
            'affiliationForm' => $form->createView(),
        ]));
    }
}
