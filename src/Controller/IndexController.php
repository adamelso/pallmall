<?php

namespace PallMall\Controller;

use PallMall\Form\Type\AuthorType;
use PallMall\HttpMessage\ResponseFactory;
use PallMall\Model\Author;
use PallMall\Repository\AuthorRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

class IndexController
{
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var AuthorRepository
     */
    private $authorRepository;

    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * @param \Twig_Environment $twig
     * @param FormFactoryInterface $formFactory
     * @param AuthorRepository $authorRepository
     * @param ResponseFactory $responseFactory
     */
    public function __construct(\Twig_Environment $twig, FormFactoryInterface $formFactory, AuthorRepository $authorRepository, ResponseFactory $responseFactory)
    {
        $this->twig = $twig;
        $this->formFactory = $formFactory;
        $this->authorRepository = $authorRepository;
        $this->responseFactory = $responseFactory;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request)
    {
        // @todo Custom repository method.
        $authors = $this->authorRepository->findBy([], ['lastName' => 'asc']);
        $form = $this->formFactory->create(AuthorType::class, new Author());

        return $this->responseFactory->createHtmlResponse($this->twig->render('index.html.twig', [
            'coAuthorForm' => $form->createView(),
            'authors' => $authors,
            'resetDemoForm' => $this->createResetDemoForm()->createView(),
        ]));
    }

    /**
     * @return FormInterface
     */
    private function createResetDemoForm()
    {
        $form = $this->formFactory->createNamed('reset_demo', FormType::class);
        $form->add('reset', SubmitType::class, ['attr' => ['class' => 'btn-danger']]);

        return $form;
    }
}
