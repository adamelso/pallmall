<?php

namespace PallMall\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use PallMall\Form\Type\AffiliationType;
use PallMall\HttpMessage\ResponseFactory;
use PallMall\Model\Affiliation;
use PallMall\Repository\AffiliationRepository;
use Symfony\Bridge\PsrHttpMessage\HttpMessageFactoryInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\VarDumper\VarDumper;

class UpdateAffiliationController
{
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var AffiliationRepository
     */
    private $repository;
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * @param \Twig_Environment $twig
     * @param FormFactoryInterface $formFactory
     * @param ObjectManager $objectManager
     * @param AffiliationRepository $repository
     * @param ResponseFactory $responseFactory
     */
    public function __construct(\Twig_Environment $twig, FormFactoryInterface $formFactory, ObjectManager $objectManager, AffiliationRepository $repository, ResponseFactory $responseFactory)
    {
        $this->twig = $twig;
        $this->formFactory = $formFactory;
        $this->repository = $repository;
        $this->objectManager = $objectManager;
        $this->responseFactory = $responseFactory;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function __invoke(Request $request)
    {
        $id = $request->attributes->get('id');

        /** @var Affiliation $affiliation */
        $affiliation = $this->repository->find($id);

        if (! $affiliation) {
            throw new NotFoundHttpException('Affiliation not found.');
        }

        $form = $this->formFactory->create(AffiliationType::class, $affiliation);

        $form->submit($request->request->get($form->getName()));

        if (! $form->isSubmitted()) {
            return new Response('Form was not submitted. ' . $form->getErrors(), 422);

        }

        if (! $form->isValid()) {
            return new Response('Form was not valid. ' . $form->getErrors(true), 422);
        }

        $this->objectManager->flush();

        return new Response($this->twig->render('affiliation.html.twig', ['affiliation' => $affiliation]));
    }
}