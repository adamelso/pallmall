<?php

namespace PallMall\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use PallMall\Form\Type\AuthorType;
use PallMall\Repository\AuthorRepository;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CreateAuthorController
{
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var AuthorRepository
     */
    private $repository;

    /**
     * @param \Twig_Environment $twig
     * @param FormFactoryInterface $formFactory
     * @param ObjectManager $objectManager
     * @param AuthorRepository $repository
     */
    public function __construct(\Twig_Environment $twig, FormFactoryInterface $formFactory, ObjectManager $objectManager, AuthorRepository $repository)
    {
        $this->twig = $twig;
        $this->formFactory = $formFactory;
        $this->objectManager = $objectManager;
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function __invoke(Request $request)
    {
        $form = $this->formFactory->create(AuthorType::class);

        $form->handleRequest($request);

        if (! $form->isSubmitted()) {
            // This shouldn't happen and probably indicates an error on our side, hence the HTTP 500.

            return new Response('Form could not be submitted.', 500);
        }

        if (! $form->isValid()) {
            // HTTP 422 - Unprocessable Entity - https://tools.ietf.org/html/rfc4918#section-11.2

            return new Response($this->twig->render('create_author_form.html.twig', ['coAuthorForm' => $form->createView()]), 422);
        }

        $author = $form->getData();

        $this->objectManager->persist($author);
        $this->objectManager->flush();

        $authors = $this->repository->findAll();

        return new Response($this->twig->render('authors.html.twig', ['authors' => $authors]));
    }
}
