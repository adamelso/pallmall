<?php

namespace PallMall\Command;

use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ResetCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('reset');
        $this->setDescription('Reset the database.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $purger = $this->getContainer()->get(ORMPurger::class);
        $purger->purge();

        $output->writeln('<info>Database reset.</info>');
    }
}
