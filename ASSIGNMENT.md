Veruscript Technical Test
=========================

Veruscript is a London-based, academic publisher with a mission to make publishing fairer. Our end-to-end publishing
service enables universities and societies to run their own journals and presses at a fraction of the normal cost.
We seek to improve the publishing process by utilising new technologies and workflows to provide the best service to
researchers. As part of this we are developing our own manuscript submission and peer review system.

During the manuscript submission process, the submitting author should be able to add co- authors in order to show them
on the manuscript record.

Code Test
---------

We’d like the candidate to build a form that allows the submitting author to add new co- authors. Each co-author must
have the following fields:

 * Name (required)
 * Last name (required)
 * Email (format email)
 * Affiliation (one or more required)
   - Institution Name (required)
   - Address (optional)
   - City (optional)
   - Country (optional)

The submitting author should be able, without refreshing the page, to:

 * Add one or more affiliations
 * Edit the affiliations added
 * Cancel the affiliations added

Notes
-----

The test doesn’t require the ability to edit the co-author once this has been saved in the DB.
For this test, we won’t be handling the submission, so leave the co-authors unrelated.

We understand that candidates are working under restricted time limits, so don’t feel bad if your code is not production
ready. Please state what the limitations are on the software you’re delivering and how it could be tackled if you could
spend more time on it.

The language of choice is PHP and ideally, we’d like you to use Symfony as a framework, and Bootstrap 3 for the UI (we’d
like you to use common bootstrap classes and best practices to build the webpage).
